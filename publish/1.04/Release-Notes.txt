App improvements:
- A network receiver has been added so that the route can be dynamically regenerated
- A GPS listener has been added to improve battery usage efficiency
- A timer has been added to calculate nearest markers dynamically
- graphical and UI improvements
- Custom styling added to markers
- Now regenerates route when a new marker is selected
- Current location of the user is now displayed
- Form UI Improvements and Required Fields Changed
- App Validation
- Bug Fixes