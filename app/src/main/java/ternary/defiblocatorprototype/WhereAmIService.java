package ternary.defiblocatorprototype;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class WhereAmIService extends IntentService {

    private BroadcastNotifier broadcaster;
    public static String current_address  = null;

    public WhereAmIService() {
        super("WhereAmIService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        broadcaster = new BroadcastNotifier(this);
        String dataString = intent.getDataString ();
        //"http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng + "&sensor=false"

        // https://maps.googleapis.com/maps/api/geocode/json?latlng=49.2717424,-123.2041456&key=AIzaSyAWjcdwlmIoy5-B8ZHkd7iA5sQCVwajYOY
        Ion.with(getApplicationContext()).
                load(dataString)
                .asJsonObject()
                .setCallback(
                        new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception ex, JsonObject obj) {
                                if (ex != null) {
                                    broadcaster.broadcastIntentWithState(Constants.STATE_ACTION_FAILED);
                                    Log.e (":)", ex.toString ());
                                    // error handling goes here
                                } else {
                                    if (!obj.get("status").getAsString().equalsIgnoreCase("OK"))
                                        broadcaster.broadcastIntentWithState(Constants.STATE_ACTION_FAILED);

                                    current_address = obj.get("results").getAsJsonArray ().get (0).getAsJsonObject().get ("formatted_address").getAsString ().replace (',', '\n');

                                    Log.e ("SERVICE", current_address);

                                    broadcaster.broadcastIntentWithState(Constants.STATE_ACTION_COMPLETE);
                                }
                            }
                        });
    }

}
