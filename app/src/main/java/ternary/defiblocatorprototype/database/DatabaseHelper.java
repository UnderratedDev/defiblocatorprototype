package ternary.defiblocatorprototype.database;

/**
 * Created by Yudhvir on 2017-02-11.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import ternary.defiblocatorprototype.database.schema.DaoMaster;
import ternary.defiblocatorprototype.database.schema.DaoSession;
import ternary.defiblocatorprototype.database.schema.MarkersDao;
import ternary.defiblocatorprototype.database.schema.Markers;
import ternary.defiblocatorprototype.database.schema.Version;
import ternary.defiblocatorprototype.database.schema.VersionDao;

public class DatabaseHelper {
    private static DatabaseHelper          instance;
    private SQLiteDatabase                 db;
    private DaoMaster                      daoMaster;
    private DaoSession                     daoSession;
    private MarkersDao                     markersDao;
    private VersionDao                     versionDao;
    private DaoMaster.DevOpenHelper        helper;

    private DatabaseHelper(final Context context)
    {
        openDatabaseForWriting(context);
    }

    public synchronized static DatabaseHelper getInstance(final Context context)
    {
        if(instance == null)
        {
            instance = new DatabaseHelper(context);
        }

        return (instance);
    }

    public static DatabaseHelper getInstance()
    {
        if (instance == null)
        {
            throw new Error();
        }

        return (instance);
    }

    private void openDatabase()
    {
        daoMaster  = new DaoMaster (db);
        daoSession = daoMaster.newSession ();
        markersDao = daoSession.getMarkersDao ();
        versionDao = daoSession.getVersionDao ();
    }

    public void openDatabaseForWriting(final Context context) {
        helper = new DaoMaster.DevOpenHelper(context, "Defiblocator.db", null);
        db     = helper.getWritableDatabase();
        openDatabase();
    }

    public void openDatabaseForReading(final Context context) {
        final DaoMaster.DevOpenHelper helper;

        helper = new DaoMaster.DevOpenHelper(context,
                "Defiblocator.db",
                null);
        db = helper.getReadableDatabase();
        openDatabase();
    }

    public void close()
    {
        helper.close();
    }

    public void createMarkerObjFromArray (Markers[] arr) {
        markersDao.insertInTx (arr);
    }

    /*
    public Markers getMarkerByObjectTitle(final String nm) {
        final List<Markers> markers;
        final Markers       marker;

        markers = markersDao.queryBuilder().where(MarkersDao.Properties.Title.eq(nm)).limit(1).list();

        if(markers.isEmpty())
        {
            marker = null;
        }
        else
        {
            marker = markers.get(0);
        }

        return marker;
    } */

    public Markers createMarker (final double lat, final double lng, final String name, final String address,
                                 final String addressone, final String city, final String country,
                                 final String province, final String postalcode, final String contactfname,
                                 final String contactlname, final String contactnumber, final String submitfname,
                                 final String submitlname, final String submitnumber) {
        // if (getMarkerByObjectTitle (title) != null)
           // return null;

        final Markers data;

        data = new Markers (null, lat, lng, name, address, addressone, city, country, province, postalcode, contactfname, contactlname, contactnumber, submitfname, submitlname, submitnumber);

        markersDao.insertOrReplace (data);

        return data;
    }

    public void insertMarker (final Markers m) {
        markersDao.insertOrReplace (m);
    }

    /*
    public void insertList (List<String> lst) {
        List<Markers> markersList = new ArrayList<Markers> ();
        for (String s : lst)
            wordLst.add (new Words(null, s));
        wordsDao.insertInTx (wordLst);
    } */

    public Markers getMarkerFromCursor (final Cursor cursor) {
        return markersDao.readEntity(cursor, 0);
    }

    public List<Markers> get_markers ()
    {
        return (markersDao.loadAll());
    }

    public Cursor getMarkersCursor () {

        /*
        final Cursor cursor;

        cursor = db.query(markersDao.getTablename(),
                markersDao.getAllColumns(),
                null,
                null,
                null,
                null,
                null);

        Log.e ("DATABASE", "" + getNumberOfMarkers());

        return (cursor); */

        Log.e ("DATABASE", "" + getNumberOfMarkers());

        return db.query(markersDao.getTablename(),
                markersDao.getAllColumns(),
                null,
                null,
                null,
                null,
                null);
    }

    public static void upgrade(final Database db,
                               final int      oldVersion,
                               final int      newVersion)
    {
    }

    public String getDatabaseName () {
        return helper.getDatabaseName ();
    }

    public void delete_all_markers () {
        db.execSQL("delete from "+ "Markers");
    }

    public long getNumberOfMarkers () { return (markersDao.count ()); }

    public void insert_version (final String version) {
        versionDao.insertOrReplace (new Version(null, version));
    }

    public void insert_version (final Version version) {
        versionDao.insertOrReplace (version);
    }

    public Cursor check_table_exists_cursor (final String table_name) {
        // return db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"
           //     + table_name + "'", null);
        return db.rawQuery("SELECT * FROM sqlite_master WHERE name =\'" + table_name + "\'" + "and type=\'table\';", null);
    }

    public boolean check_table_exists (final String table_name) {
        return check_table_exists_cursor(table_name).getCount () > 0;
    }

    public Cursor getVersionCursor () {
        final Cursor cursor;

        cursor = db.query(versionDao.getTablename(),
                versionDao.getAllColumns(),
                null,
                null,
                null,
                null,
                null);

        return (cursor);
    }

    public Version getVersionFromCursor (final Cursor cursor) {
        if (!cursor.moveToFirst ())
            return null;
        return versionDao.readEntity(cursor, 0);
    }

    public void delete_all_versions () {
        db.execSQL("delete from "+ "Version");
    }
}