package ternary.defiblocatorprototype;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

import ternary.defiblocatorprototype.database.schema.Markers;

/**
 * Created by Yudhvir on 15/06/17.
 */

public class MarkersObj implements ClusterItem {

    private MarkerOptions mo;
    private Marker        ma;
    private Markers       m;
    private BitmapDescriptor previous, current;
    protected static final BitmapDescriptor icon_default = BitmapDescriptorFactory.fromResource (R.drawable.icon_default), icon_selected = BitmapDescriptorFactory.fromResource (R.drawable.icon_selected), icon_target = BitmapDescriptorFactory.fromResource (R.drawable.icon_target);

    public MarkersObj (Markers m) {
        this.m  = m;
        this.mo = new MarkerOptions ()
                .title (m.getName ())
                .snippet (m.getAddress ())
                .position (new LatLng(m.getLat(), m.getLng()))
                .icon (icon_default);
                // .icon (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        // this.current = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
        this.current = icon_default;
    }

    public MarkersObj (Markers m, BitmapDescriptor icon) {
        this.m  = m;
        this.mo = new MarkerOptions ()
                .title (m.getName ())
                .snippet (m.getAddress ())
                .position (new LatLng(m.getLat(), m.getLng()))
                .icon (icon);
        this.current = icon;
    }

    public MarkerOptions getMarkerOption () {
        return mo;
    }

    public Markers getMarkersObject () {
        return m;
    }

    public void setMarker (final Marker m) {
        this.ma = m;
    }

    public void setMarkerDefault (final Marker m) {
        this.ma = m;
    }

    public Marker getMarker () {
        return ma;
    }

    public void setMarkerOption (final MarkerOptions mo) {
        this.mo = mo;
    }

    public void setMarker (final Markers m) {
        this.m = m;
    }

    public void setMarkerDefault (final Markers m) {
        this.m = m;
        changeIcon (icon_default);
    }

    public void removeMarker () {
        if (this.ma != null)
            this.ma.remove ();
        this.ma = null;
    }

    public void setIcon (final BitmapDescriptor icon) {
        this.previous = this.current;
        this.current  = icon;
    }

    public BitmapDescriptor getPreviousIcon () {
        return this.previous;
    }

    public BitmapDescriptor getCurrentIcon () {
        return this.current;
    }

    /*
    public void changeIcon (final BitmapDescriptor icon) {

        if (mo == null || ma == null || icon == null)
            return;

        this.previous = current;
        mo.icon (icon);
        // Log.e (":)", ma.toString ());

        if (ma != null && ma.isVisible())
            ma.setIcon(icon);

        this.current = icon;
    } */

    public void changeIcon (final BitmapDescriptor icon) {

        if (mo == null || icon == null || this.current == icon)
            return;

        this.previous = current;
        mo.icon (icon);

        this.current = icon;
    }

    /*
    private void changeIcon (final BitmapDescriptor icon) {
        if (mo == null)
            return;
        this.previous = current;
        mo.icon (icon);
        this.current = icon;
    } */

    public void changeIconPrevious () {
        changeIcon (this.previous);
    }

    public void changeIconDefault () {
        // changeIcon (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        changeIcon (icon_default);
    }

    /*
    public void changeIconGreen () {
        changeIcon (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
    }

    public void changeIconPurple () {
        changeIcon (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
    } */

    public void changeIconSelected () {
        changeIcon (icon_selected);
    }

    public void changeIconTarget () {
        // changeIcon (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
        changeIcon (icon_target);
    }

    public void showInfoWindow () {
        if (this.ma != null)
            this.ma.showInfoWindow ();
    }

    @Override
    public LatLng getPosition() {
        return mo.getPosition ();
    }

    @Override
    public String getTitle() {
        return mo.getTitle ();
    }

    @Override
    public String getSnippet() {
        return mo.getSnippet ();
    }

}
