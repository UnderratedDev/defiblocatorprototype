package ternary.defiblocatorprototype;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SubmitAedFragment extends Fragment {

    private SubmitAedFragment.SubmitAedServiceReceiver submit_aed_reciever;
    private boolean use_user_location;
    private View view;
    private LinearLayout ll;

    public SubmitAedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SubmitAedFragment.
     */
    public static SubmitAedFragment newInstance() {
        SubmitAedFragment fragment = new SubmitAedFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        use_user_location = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_submit_aed, container, false);
    }

    @Override
    public void onActivityCreated (final Bundle args) {
        super.onActivityCreated (args);


        view = getView ();

        if (view != null) {

            ll   = view.findViewById (R.id.form);

            view.findViewById(R.id.submitAedbtn).setOnClickListener(new View.OnClickListener() {
                public void onClick(final View view) {
                    submitAed(view);
                }
            });

        /*
            CheckBox checkbox = view.findViewById (R.id.checkbox_location);

            checkbox.setOnCheckedChangeListener (new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                    use_user_location = b;

                    if (use_user_location)
                        add_address_fields ();
                    else
                        remove_address_fields ();

                }

            }); */
        }
    }

    private void remove_address_fields () {
        View v = view.findViewById (R.id.addressText);
        if (v != null) {
            v.setVisibility(View.GONE);
            ll.removeView(v);
            ll.invalidate ();
        }
    }

    private void add_address_fields () {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        } */
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private boolean checkLocationPermission () {
        return (getActivity ().checkCallingOrSelfPermission ("android.permission.ACCESS_FINE_LOCATION") == PackageManager.PERMISSION_GRANTED);
    }

    public void submitAed (final View view) {
        // Check if null
        // String name = ((EditText) getActivity().findViewById(R.id.name)).getText ().toString (), fnameuser = ((EditText) getActivity().findViewById(R.id.f_submit)).getText ().toString (), lnameuser = ((EditText) getActivity().findViewById(R.id.l_submit)).getText ().toString (), phoneUser = ((EditText) getActivity().findViewById(R.id.phoneUser)).getText ().toString (), aed_fname = ((EditText) getActivity().findViewById(R.id.f_owner)).getText ().toString (), aed_lname = ((EditText) getActivity().findViewById(R.id.l_owner)).getText ().toString (), emerPhoneUser = ((EditText) getActivity().findViewById(R.id.emerphoneUser)).getText ().toString ();
        // Toast.makeText (getBaseContext (), nameUser.getText() + " " + phoneUser.getText() + " " + aedOwner.getText() + " " + emerPhoneUser.getText() + " " + locDetails.getText(), Toast.LENGTH_SHORT).show ();

        Editable name              = ((EditText) getActivity().findViewById(R.id.name)).getText (),
                 fnameuser         = ((EditText) getActivity().findViewById(R.id.f_submit)).getText (),
                 lnameuser         = ((EditText) getActivity().findViewById(R.id.l_submit)).getText (),
                 phoneUser         = ((EditText) getActivity().findViewById(R.id.phoneUser)).getText (),
                 aed_fname         = ((EditText) getActivity().findViewById(R.id.f_owner)).getText (),
                 aed_lname         = ((EditText) getActivity().findViewById(R.id.l_owner)).getText (),
                 emerPhoneUser     = ((EditText) getActivity().findViewById(R.id.emerphoneUser)).getText (),
                 addressUser       = ((EditText) getActivity().findViewById(R.id.addressUser)).getText (),
                 addressOneUser    = ((EditText) getActivity().findViewById(R.id.addressOneUser)).getText (),
                 cityUser          = ((EditText) getActivity().findViewById(R.id.cityUser)).getText (),
                 postcodeUser      = ((EditText) getActivity().findViewById(R.id.postcodeUser)).getText (),
                 provinceStateUser = ((EditText) getActivity().findViewById(R.id.provinceStateUser)).getText (),
                 countryUser       = ((EditText) getActivity().findViewById(R.id.countryUser)).getText ();

        if (name == null || name.toString ().isEmpty () || addressUser == null || addressUser.toString ().isEmpty () || countryUser == null || countryUser.toString ().isEmpty ()) {
            Toast.makeText (getContext (), "Please fill in the required fields (Name, Address, Country)", Toast.LENGTH_LONG).show ();
            return;
        }

        // Use dialog box
        if (!checkLocationPermission())
            System.exit(1);


        String nameStr = name.toString (), addressUserStr = addressUser.toString (), countryUserStr = countryUser.toString (),
                fnameuserStr = null, lnameuserStr = null, phoneUserStr = null, aed_fnameStr = null, aed_lnameStr = null, emerPhoneUserStr = null, addressOneUserStr = null, cityUserStr = null, postcodeUserStr = null, provinceStateStr = null;

        if (fnameuser != null)
            fnameuserStr = fnameuser.toString ();

        if (lnameuser != null)
            lnameuserStr = lnameuser.toString ();

        if (phoneUser != null)
            phoneUserStr = phoneUser.toString ();

        if (aed_fname != null)
            aed_fnameStr = aed_fname.toString ();

        if (aed_lname != null)
            aed_lnameStr = aed_lname.toString ();

        if (emerPhoneUser != null)
            emerPhoneUserStr = emerPhoneUser.toString ();

        if (addressOneUser != null)
            addressOneUserStr = addressOneUser.toString ();

        if (cityUser != null)
            cityUserStr = cityUser.toString ();

        if (postcodeUser != null)
            postcodeUserStr = postcodeUser.toString ();

        if (provinceStateUser != null)
            provinceStateStr = provinceStateUser.toString ();


        /*
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Get Current Location
        Location currentLoc = locationManager.getLastKnownLocation(provider); */

        String url = ("http://defiblocator.ca/api/send_email_user_create_marker.php?name=" + Encoder.encode (nameStr) + "&address=" + Encoder.encode (addressUserStr) + "&addressone=" + Encoder.encode (addressOneUserStr) + "&city=" + Encoder.encode (cityUserStr) + "&province" + Encoder.encode (provinceStateStr) + "&postalcode=" + Encoder.encode (postcodeUserStr) + "&country=" + Encoder.encode (countryUserStr) + "&contactnumber=" + Encoder.encode (emerPhoneUserStr)).replace ("\n", "");

        Log.e (":)", url);

        Intent mServiceIntent = new Intent (getActivity (), SubmitAedService.class);
        mServiceIntent.setData (Uri.parse (url));
        getActivity ().startService (mServiceIntent);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.BROADCAST_ACTION);
        submit_aed_reciever = new SubmitAedFragment.SubmitAedServiceReceiver();

        LocalBroadcastManager.getInstance(getActivity ()).registerReceiver(submit_aed_reciever, intentFilter);
        // Get title from user, generate one or have a general one (AED)
        // helper.createMarker(currentLoc.getLatitude(), currentLoc.getLongitude(), "Marker",locDetails.getText ().toString ());
    }

    private class SubmitAedServiceReceiver extends BroadcastReceiver {

        private SubmitAedServiceReceiver() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra (Constants.EXTENDED_DATA_STATUS, Constants.STATE_ACTION_CONNECTING);
            Toast.makeText (getActivity (), status == Constants.STATE_ACTION_COMPLETE ? "Success" : "Unable to submit",
                    Toast.LENGTH_LONG).show ();
        }
    }
}
