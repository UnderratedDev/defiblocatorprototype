package ternary.defiblocatorprototype;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by yudhvirraj on 2017-09-11.
 */

public class ProgressWebViewClient extends WebViewClient {

    private ProgressBar progressBar;

    public ProgressWebViewClient(ProgressBar progressBar) {
        this.progressBar = progressBar;
        progressBar.setVisibility (View.VISIBLE);
    }

    @Override
    public boolean shouldOverrideUrlLoading (WebView webView, String url) {
        webView.loadUrl (url);
        return true;
    }

    @Override
    public void onPageFinished (WebView webView, String url) {
        super.onPageFinished (webView, url);
        progressBar.setVisibility (View.GONE);
    }

}
