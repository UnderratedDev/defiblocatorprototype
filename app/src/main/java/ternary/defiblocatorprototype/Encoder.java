package ternary.defiblocatorprototype;

import android.util.Base64;
import android.util.Log;

/**
 * Created by Yudhvir on 03/08/2017.
 */

public class Encoder {
    public static String encode (final String word) {
        try {
            return Base64.encodeToString(word.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception ex){
            Log.e (":)", ex.toString ());
            return null;
        }

    }
}
