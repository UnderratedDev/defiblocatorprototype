package ternary.defiblocatorprototype;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

/**
 * Created by Yudhvir on 10/05/2017.
 */

public class SubmitAedService extends IntentService {

    private BroadcastNotifier broadcaster;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * name Used to name the worker thread, important only for debugging.
     */
    public SubmitAedService() {
        super("SubmitAedService");
    }

    @Override
    protected void onHandleIntent (Intent workIntent) {
        broadcaster = new BroadcastNotifier(this);
        String dataString = workIntent.getDataString ();

        Log.e ("Background Service", dataString);
        Ion.with(getApplicationContext()).
                load(dataString).
                asJsonObject()
                .setCallback(
                        new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject json) {
                                if (e != null) {
                                    Toast.makeText (getApplicationContext (), e.toString (), Toast.LENGTH_SHORT).show ();
                                    Log.e (":)", e.toString ());
                                    broadcaster.broadcastIntentWithState(Constants.STATE_ACTION_FAILED);
                                    // error handling goes here
                                } else {

                                    String status = json.get("status").getAsString();

                                    broadcaster.broadcastIntentWithState(status.equalsIgnoreCase ("success") ? Constants.STATE_ACTION_COMPLETE : Constants.STATE_ACTION_FAILED);
                                }
                            }
                        }
                );

    }
}
