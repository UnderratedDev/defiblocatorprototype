package ternary.defiblocatorprototype;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yudhvirraj on 2017-09-03.
 */

public class ClusterRenderer extends DefaultClusterRenderer<MarkersObj> {

    private ClusterManager <MarkersObj> mClusterManager;
    private MarkerCache<MarkersObj> mMarkerCache = new MarkerCache<>();

    public ClusterRenderer (final Context context, final GoogleMap map, ClusterManager<MarkersObj> clusterManager) {
        super (context, map, clusterManager);
        clusterManager.setRenderer (this);
        mClusterManager = clusterManager;
    }

    @Override
    protected void onBeforeClusterItemRendered (MarkersObj m, MarkerOptions mo) {
        final BitmapDescriptor b = m.getCurrentIcon ();
        if (b != null)
            mo.icon (b);
        mo.visible (true);
    }

    @Override
    protected void onClusterItemRendered (MarkersObj mo, Marker m) {
        super.onClusterItemRendered (mo, m);
        // if (mo != null && mo.getCurrentIcon() != null)

        if (m != null && m.isVisible() && mo != null && mo.getCurrentIcon() != null)
            m.setIcon(mo.getCurrentIcon());

        mMarkerCache.put (mo, m);
    }

    @Override
    protected void onClusterRendered (Cluster<MarkersObj> cluster, Marker marker) {
        super.onClusterRendered (cluster, marker);
    }

    /**
     * A cache of markers representing individual ClusterItems.
     */
    private static class MarkerCache<T> {
        private Map<T, Marker> mCache = new HashMap<T, Marker>();
        private Map<Marker, T> mCacheReverse = new HashMap<Marker, T>();

        public Marker get(T item) {
            return mCache.get(item);
        }

        public T get(Marker m) {
            return mCacheReverse.get(m);
        }

        public void put(T item, Marker m) {
            mCache.put(item, m);
            mCacheReverse.put(m, item);
        }

        public void remove(Marker m) {
            T item = mCacheReverse.get(m);
            mCacheReverse.remove(m);
            mCache.remove(item);
        }
    }

    public Marker getMarker(MarkersObj clusterItem) {
        return mMarkerCache.get(clusterItem);
    }

}
