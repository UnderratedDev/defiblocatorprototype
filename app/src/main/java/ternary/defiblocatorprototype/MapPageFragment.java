package ternary.defiblocatorprototype;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import ternary.defiblocatorprototype.database.schema.Markers;

public class MapPageFragment extends android.support.v4.app.Fragment {// implements View.OnClickListener{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private Context  mContext;
    private Activity activity;

    private final static int MY_PERMISSIONS_REQUEST_ACCESS_CALL_PHONE = 3;
    public static boolean load_all_markers    = false;
    protected static boolean location_updated = false;
    private Cursor aed_cursor;
    private Handler location_interval_handler;
    private Handler network_handler;
    private HandlerThread location_thread;
    private HandlerThread handler_thread;
    private Looper network_looper;
    private Runnable runnable, workerFetchRunnable, closestMarkerRunnable, progressDialogRunnable;
    private Thread workerFetch, workerClosestMarker;

    // private GpsStatus.Listener gps_status_listener;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private PolylineOptions lineOptions;
    private Polyline route;
    protected static ClusterManager<MarkersObj>  mClusterManager;
    private ClusterRenderer mClusterRenderer;
    protected static ArrayList<MarkersObj> markers;
    private MarkersObj aed_marker;
    private MarkersObj closestMarker;
    private MapPageFragment.ParserTask parserTask;
    private MapPageFragment.BackendPullServiceReceiver backendReceiver;
    private MapPageFragment.WhereAmIServiceReceiver    whereAmIReceiver;

    private ProgressDialog progress;

    private boolean closest_marker_lock = false;

    private LocationManager location_manager;

    private long last_closest_run_timer_markers;

    /**
     * The desired interval for the gps checker and the route update. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_GPS_LOCATION_CHECK_IN_MILLISECONDS = 720000;

    /**
     * The desired interval for the gps checker if location is null and the route update. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_GPS_LOCATION_NULL_CHECK_IN_MILLISECONDS = 5000;

    /**
     * The desired interval for the thread to sleep for network connection check. Inexact, Updates may be more or less frequent
     */
    private static final long UPDATE_NETWORK_SLEEP_CHECK_IN_MILLISECONDS = 500;

    /**
     * The desired timeout for waiting on user location
     */
    private static final long USER_LOCATION_TIMEOUT = 800;

    /**
     * The desired amount of markers to send to google to find closest marker
     */
    private final int CLOSEST_MARKER_COMPARISON_SIZE = 5;

    /**
     * The desired interval to wait before you make the closest_marker calculation
     */
    private final int TIME_BEFORE_NEXT_RUN_TIMER_MARKERS_CALCULATION = 3000;

    private Button   dir_btn;
    private TextView where_am_i;

    private PriorityQueue<HashMap<MarkersObj, Float>> closestMarkers = new PriorityQueue<>(1, new Comparator<HashMap<MarkersObj, Float>>() {

        @Override
        public int compare(HashMap<MarkersObj, Float> map, HashMap<MarkersObj, Float> map1) {
            Map.Entry<MarkersObj, Float> entry = map.entrySet().iterator().next(), entry1 = map1.entrySet().iterator().next();

            return map.get(entry.getKey()).compareTo(map1.get(entry1.getKey()));
        }
    });

    public MapPageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment MapPageFragment.
     */
    public static MapPageFragment newInstance() {
        MapPageFragment fragment = new MapPageFragment();
        return fragment;
    }

    // Add reload button
    // Check for multiple markers in same location
    // Use on Resume? check other life cycle methods.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        markers = new ArrayList<>();
        location_interval_handler = new Handler();
        handler_thread = new HandlerThread("ht");
        handler_thread.start();
        last_closest_run_timer_markers = -100000000;
        runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (isAdded() && NavigationDrawerActivity.mRequestingLocationUpdates) {
                        // Log.e ("TAG", "" + location_updated);
                        // Log.e ("TAG", "" + NavigationDrawerActivity.mCurrentLocation);
                        if (location_updated && NavigationDrawerActivity.mCurrentLocation != null && mContext != null && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) && location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            run_timer_markers ();
                            Log.e ("LOCATION", "READY");
                            location_updated = false;
                        }
                    }

                    /* try {

                        if (!location_updated && (NavigationDrawerActivity.mCurrentLocation == null || route == null) && NavigationDrawerActivity.mRequestingLocationUpdates)
                            Thread.sleep (UPDATE_GPS_LOCATION_CHECK_IN_MILLISECONDS);
                        // Thread.sleep((NavigationDrawerActivity.mCurrentLocation == null || route == null) && NavigationDrawerActivity.mRequestingLocationUpdates ? UPDATE_GPS_LOCATION_NULL_CHECK_IN_MILLISECONDS : UPDATE_GPS_LOCATION_CHECK_IN_MILLISECONDS);
                    } catch (Exception ex) {

                    } */
                }
            }
        };

        // Fetches the data and calls the get_closest_bin method to calculate the nearest bin.
        // Overrides the on marker click listener to show and hide the donate qty button when required.
        workerFetchRunnable = new Runnable() {

            @Override
            public synchronized void run() {

                if (map != null) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            clear_clusters();
                        }
                    });

                    // extractCursorData();

                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            addMarkersToMap();
                        }
                    });

                    if (mContext != null && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                        run_timer_markers();
                }
            }

        };

        closestMarkerRunnable = new Runnable() {

            @Override
            public synchronized void run() {
                closest_marker_setup();
            }

        };

        progressDialogRunnable = new Runnable() {

            public synchronized void run() {
                if (progress != null)
                    progress.cancel();
                progress = new ProgressDialog(mContext);
                progress.setTitle("Loading");
                progress.setMessage("Wait while loading...");
                progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
                progress.show();
            }

        };

        location_thread = new HandlerThread("HandlerThread");
        location_thread.start();
        location_interval_handler = new Handler(location_thread.getLooper());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map_page, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        Log.e (":)", "ATTACHED");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.e (":)", "ATTACHED");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e (":)", "DETACHED");
        mContext = null;
    }

    @Override
    public void onDestroy () {
        super.onDestroy ();
        if (progress != null) {
            progress.dismiss ();
            progress = null;
        }
        Log.e(":)", "DESTROYED");
    }

    @Override
    public void onDestroyView () {
        super.onDestroyView ();
        Log.e (":)", "DESTROYED VIEW");
        try {
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(backendReceiver);
        } catch (Exception ex) {

        }
    }

    @Override
    public void onActivityCreated(Bundle args) {
        super.onActivityCreated(args);

        activity = getActivity ();

        Log.e(":)", "ACTIVITY CREATED");
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);

        createMap();

        dir_btn = activity.findViewById(R.id.directionBtn);

        where_am_i = activity.findViewById(R.id.where_am_i);

        location_manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        if (check_location_permission())
            Toast.makeText(mContext, "Location not enabled", Toast.LENGTH_LONG).show();

        if (!checkCallPermission())
            requestPermissions(new String[] { Manifest.permission.CALL_PHONE }, MY_PERMISSIONS_REQUEST_ACCESS_CALL_PHONE);

        final View view = getView();

        if (view != null)
            view.findViewById(R.id.callBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    if (aed_marker == null) {
                        Toast.makeText(mContext, "No Number Available", Toast.LENGTH_LONG).show();
                        return;
                    }
                    callIntent.setData(Uri.parse("tel:" + aed_marker.getMarkersObject().getContact_number()));
                    if (checkCallPermission())
                        startActivity(callIntent);
                }
            });

        dir_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (aed_marker != null) {
                    LatLng latLng = aed_marker.getMarkerOption().getPosition();
                    String url = NavigationDrawerActivity.mCurrentLocation != null ? "http://maps.google.com/maps?saddr=" + NavigationDrawerActivity.mCurrentLocation.getLatitude() + "," + NavigationDrawerActivity.mCurrentLocation.getLongitude() + "&daddr=" + latLng.latitude + "," + latLng.longitude : "http://maps.google.com/maps?daddr=" + latLng.latitude + "," + latLng.longitude;
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
            }
        });

    }

    public synchronized void switch_change_markers () {

        activity.runOnUiThread (new Runnable () {

            @Override
            public void run () {
                if (route != null)
                    route.remove ();
            }

        });

        aed_marker = null;
        load_page ();
    }

    private void load_page () {
        pullDataFromServer();
    }

    private BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = NetworkUtil.getConnectivityStatusString(context);
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                    // location_interval_handler.removeCallbacksAndMessages(null);
                    Toast.makeText (context, "Network connectivity lost", Toast.LENGTH_LONG).show ();
                } else {
                    if (NetworkUtil.check_connection (context) != 0) {
                        // Look for better solution in future
                        long startTime = System.currentTimeMillis (), finishTime = 0;

                        while (NetworkUtil.check_connection (context) == 2 && (finishTime = System.currentTimeMillis () - startTime) < UPDATE_NETWORK_SLEEP_CHECK_IN_MILLISECONDS)
                            Log.e ("Network", "Waiting for connection");

                        if (finishTime >= UPDATE_NETWORK_SLEEP_CHECK_IN_MILLISECONDS)
                            return;

                        load_page ();
                        Toast.makeText(context, "Network connected", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        Log.e (":)", "ON START");
    }

    private synchronized void run_timer_markers () {

        long current_time = System.currentTimeMillis ();

        if ((closestMarker == null || route == null || TIME_BEFORE_NEXT_RUN_TIMER_MARKERS_CALCULATION < (current_time - last_closest_run_timer_markers)) && map != null) {
            last_closest_run_timer_markers = System.currentTimeMillis ();
            workerClosestMarker = new Thread (closestMarkerRunnable);
            workerClosestMarker.start ();
        }

    }

    private void run_timer () {
        location_interval_handler.post (runnable);
    }

    private void stop_timer () {
        location_interval_handler.removeCallbacksAndMessages(null);
        location_interval_handler.removeCallbacks (runnable);
    }

    // Uses an anonymous inner class to implement the map
    // Creates the map,
    // Has a listener which constantly updates the cur location private variable at the top
    // Executes the background task that fetches the data and then calculates the closest marker
    private void createMap () {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;

                mClusterManager  = new ClusterManager<> (mContext, map);
                mClusterRenderer = new ClusterRenderer  (activity, map, mClusterManager);


                map.setOnCameraIdleListener(mClusterManager);
                map.setOnMarkerClickListener(mClusterManager);
                map.setOnInfoWindowClickListener(mClusterManager);

                mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<MarkersObj>() {

                    @Override
                    public boolean onClusterClick(Cluster<MarkersObj> cluster) {
                        return false;
                    }

                });

                mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MarkersObj>() {

                    @Override
                    public boolean onClusterItemClick(MarkersObj item) {
                        if (aed_marker != null) {
                            if (aed_marker.getMarker() == null)
                                aed_marker.setMarker(mClusterRenderer.getMarker(aed_marker));

                            // if (aed_marker.getMarkersObject() .getId ().equals(item.getMarkersObject ().getId ())) {
                            if (aed_marker.getPosition ().equals(item.getPosition ())) {
                                activity.runOnUiThread (new Runnable () {
                                    @Override
                                    public void run () {
                                        if (!aed_marker.getCurrentIcon ().equals (MarkersObj.icon_selected))
                                            aed_marker.changeIconSelected ();

                                        aed_marker.showInfoWindow();
                                    }
                                });
                                return true;
                            }

                            activity.runOnUiThread(new Runnable() {
                                public void run () {
                                    if (closestMarker != null && !aed_marker.getPosition ().equals (closestMarker.getPosition ()))
                                        aed_marker.changeIconPrevious();
                                    else
                                        aed_marker.changeIconTarget ();
                                }
                            });
                        }

                        aed_marker = item;

                        if (aed_marker.getMarker() == null)
                            aed_marker.setMarker(mClusterRenderer.getMarker(aed_marker));

                        activity.runOnUiThread (new Runnable () {
                            public void run () {
                                Log.e ("MARKER CLICK", "ICON CHANGE");
                                mClusterManager.removeItem(aed_marker);
                                aed_marker.changeIconSelected();
                                mClusterManager.addItem(aed_marker);
                                mClusterManager.cluster ();
                                aed_marker.showInfoWindow();
                                changeButtonText ();
                                draw_route (NavigationDrawerActivity.mCurrentLocation);
                                map.animateCamera (CameraUpdateFactory.newLatLng(aed_marker.getPosition ()), 400, null);
                            }
                        });

                        return true;
                    }

                });

                // check_location_permission method does not work here???
                if (ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    return;

                map.moveCamera (CameraUpdateFactory.newLatLngZoom (new LatLng(49.2290040, -123.0412511), 10));

                map.setMyLocationEnabled(true);

                map.getUiSettings().setMapToolbarEnabled(true);

            }
        });
    }

    // Depending on which permission has been granted, different actions occur, i.e for location, the map is initialised
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CALL_PHONE: {
            }
        }
    }

    /*
        Creates a new intent to start the BackendPullService
        IntentService. Passes a URI in the
        Intents's "data" field
    */
    private void pullDataFromServer () {

        activity.runOnUiThread (progressDialogRunnable);

        int locationGrant = 1;

        if ((ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || !location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)))
            locationGrant = 0;

        // Future, check if aed_marker is same as closest marker and if so, do not change it.

        Intent mServiceIntent = new Intent (mContext, BackendPullService.class);
        if (load_all_markers) {
            mServiceIntent.setData(Uri.parse("http://defiblocator.ca/api/get_markers.php"));
            Log.e ("URL", "http://defiblocator.ca/api/get_markers.php");
        } else {
            if (locationGrant == 1) {
                long startTime = System.currentTimeMillis();
                while (NavigationDrawerActivity.mCurrentLocation == null && (System.currentTimeMillis() - startTime) < USER_LOCATION_TIMEOUT) {}
                if (NavigationDrawerActivity.mCurrentLocation != null) {
                    // Log.e ("URL", (("http://defiblocator.ca/api/get_markers_location.php?latitude=" + Encoder.encode("" + NavigationDrawerActivity.mCurrentLocation.getLatitude()) + "&longitude=" + Encoder.encode("" + NavigationDrawerActivity.mCurrentLocation.getLongitude())).replace("\n", "")));
                    mServiceIntent.setData(Uri.parse(("http://defiblocator.ca/api/get_markers_location.php?latitude=" + Encoder.encode("" + NavigationDrawerActivity.mCurrentLocation.getLatitude()) + "&longitude=" + Encoder.encode("" + NavigationDrawerActivity.mCurrentLocation.getLongitude())).replace("\n", "")));
                } else {
                    Toast.makeText(mContext, "User Location not available", Toast.LENGTH_LONG).show();
                    mServiceIntent.setData(Uri.parse("http://defiblocator.ca/api/get_markers.php"));
                }
            } else {
                Toast.makeText(mContext, "User Location not available", Toast.LENGTH_LONG).show();
                mServiceIntent.setData(Uri.parse("http://defiblocator.ca/api/get_markers.php"));
            }
        }

        mContext.startService (mServiceIntent);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.BROADCAST_ACTION);
        backendReceiver = new MapPageFragment.BackendPullServiceReceiver();

        LocalBroadcastManager.getInstance(mContext).registerReceiver(backendReceiver, intentFilter);
    }

    private String get_distance_url (LatLng origin, ArrayList<MarkersObj> dest) {
        String str_dest = "destinations=";
        for (int i = 0; i < dest.size(); i++) {
            str_dest = str_dest + dest.get(i).getMarkerOption().getPosition().latitude + "," + dest.get(i).getMarkerOption().getPosition().longitude;
            str_dest = i != dest.size() - 1 ? str_dest + "|" : str_dest ;
        }

        String str_origin = "origins=" + origin.latitude + "," + origin.longitude, parameters = str_origin + "&" + str_dest;

        return "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&" + parameters;
    }

    // Check for location permission (FINE AND COARSE) and returns true if permission has not been granted
    private boolean check_location_permission () {
        return (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkCallPermission () {
        return (mContext.checkCallingOrSelfPermission ("android.permission.CALL_PHONE") == PackageManager.PERMISSION_GRANTED);
    }

    private void whereAmI () {
        if (NavigationDrawerActivity.mCurrentLocation == null)
            return;

        Intent mServiceIntent = new Intent (mContext, WhereAmIService.class);
        mServiceIntent.setData (Uri.parse ("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + NavigationDrawerActivity.mCurrentLocation.getLatitude () + "," + NavigationDrawerActivity.mCurrentLocation.getLongitude() + "&key=AIzaSyDonA0l1rnsrVVG89YAqI1k4C56-CsCNok"));
        mContext.startService (mServiceIntent);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.BROADCAST_ACTION);
        whereAmIReceiver = new MapPageFragment.WhereAmIServiceReceiver();

        LocalBroadcastManager.getInstance(mContext).registerReceiver(whereAmIReceiver, intentFilter);
    }


    private synchronized int get_closest_bin_online () {
        // Log.e (":)", "Waiting for turn");

        closest_marker_lock = true;

        final Location location = NavigationDrawerActivity.mCurrentLocation;

        if (location == null || markers.isEmpty())
            return 1;

        Location target = new Location ("target");

        closestMarkers.clear ();

        for (MarkersObj mo :  markers) {
            LatLng temp = mo.getMarkerOption().getPosition ();
            target.setLatitude  (temp.latitude);
            target.setLongitude (temp.longitude);
            HashMap <MarkersObj, Float>hm = new HashMap <>();
            hm.put (mo, location.distanceTo(target));
            closestMarkers.add (hm);
        }

        final ArrayList<MarkersObj> closest = new ArrayList<>();

        int no_distance_markers = 5 < closestMarkers.size () ? CLOSEST_MARKER_COMPARISON_SIZE : closestMarkers.size ();

        for (int i = 0; i < no_distance_markers; i++)
            closest.add(closestMarkers.poll().entrySet().iterator().next().getKey());

        String url = get_distance_url (new LatLng(location.getLatitude(), location.getLongitude()), closest);

        Log.e (":)", url);

        /* Redo in future to prevent java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
           at ternary.defiblocatorprototype.MapPageFragment$13.onCompleted(MapPageFragment.java:954)
           at ternary.defiblocatorprototype.MapPageFragment$13.onCompleted(MapPageFragment.java:947)
         */
        Ion.with(mContext).
                load(url).
                asJsonObject()
                .setCallback(
                        new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e != null) {
                                    closest_marker_lock = false;
                                    // error handling goes here
                                } else {
                                    try {
                                        int smallest = Integer.MAX_VALUE, index = 0;
                                        JsonArray array = result.getAsJsonObject().get("rows").getAsJsonArray().get(0).getAsJsonObject().get("elements").getAsJsonArray();

                                        if (!array.get(0).getAsJsonObject().get("status").getAsString().equalsIgnoreCase("ZERO_RESULTS")) {

                                            for (int i = 0; i < array.size(); ++i) {
                                                int current = array.get(i).getAsJsonObject().get("distance").getAsJsonObject().get("value").getAsInt();
                                                if (current < smallest) {
                                                    smallest = current;
                                                    index = i;
                                                }
                                            }

                                            closestMarker = markers.get(getMarker(closest.get(index)));
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace ();
                                        Log.e (":)", "Exceeded Limit!");
                                        int index = get_closest_bin();
                                        if (index != -1)
                                            closestMarker = markers.get(index);
                                    }
                                }
                                closest_marker_lock = false;
                            }
                        }
                );
        return 0;
    }

    // Calculates the closest bin relative to cur user position/location
    private int get_closest_bin () {
        int index = -1;
        float minDistance = Float.MAX_VALUE;
        final Location location = NavigationDrawerActivity.mCurrentLocation;

        if (location == null || markers.isEmpty())
            return index;

        if (0 < markers.size ()) {
            Location target = new Location ("target");
            for (int i = 0; i < markers.size(); ++i) {
                LatLng temp = markers.get (i).getMarkerOption().getPosition ();
                target.setLatitude  (temp.latitude);
                target.setLongitude (temp.longitude);
                if (location.distanceTo (target) < minDistance) {
                    minDistance = location.distanceTo(target);
                    index = i;
                }
            }
        }
        return index;
    }

    private void changeButtonText () {
        if (aed_marker == null)
            return;

        dir_btn.setText(aed_marker.getMarkersObject ().getName());
    }

    private void ChangeWhereAmIText () {
        if (WhereAmIService.current_address != null && !WhereAmIService.current_address.isEmpty ())
            where_am_i.setText ("CURRENT LOCATION:\n" + WhereAmIService.current_address);
    }

    private int getMarker (final LatLng position) {
        for (int i = 0; i < markers.size (); ++i)
            if (markers.get(i).getMarkerOption ().getPosition ().equals (position))
                return i;
        return -1;
    }

    // Returns the index of a markers obj in the markers list
    private int getMarker (final MarkersObj m) {
        for (int i = 0; i < markers.size (); ++i)
            if (markers.get(i).getPosition ().equals (m.getPosition ()))
                return i;
        return -1;
    }

    private void clear_clusters () {
        if (markers == null || markers.isEmpty ())
            return;

        // for (MarkersObj m : markers)
           // mClusterManager.removeItem (m);

        mClusterManager.clearItems ();

        mClusterManager.cluster ();
    }

    private synchronized void extractCursorData () {
        Log.e ("URL", "EXTRACTING");
        markers = new ArrayList<>();
        Log.e ("URL", "HELPER SIZE : " + NavigationDrawerActivity.helper.getNumberOfMarkers ());
        // Log.e ("URL", "CURSOR SIZE B : " + aed_cursor.getCount());
        for (aed_cursor.moveToFirst(); !aed_cursor.isAfterLast (); aed_cursor.moveToNext ()) {
            final Markers marker = NavigationDrawerActivity.helper.getMarkerFromCursor (aed_cursor);
            markers.add (new MarkersObj (marker));
        }

        Log.e ("URL", "MARKER SIZE : " +  markers.size ());
        // Log.e ("URL", "CURSOR SIZE A : " + aed_cursor.getCount());
        aed_cursor = null;
    }

    private void removeAllMarkers () {
        for (MarkersObj m : markers)
            m.removeMarker ();
    }

    private void addMarkersToMap () {
        // removeAllMarkers ();

        if (markers == null || markers.isEmpty ()) {
            progress.dismiss ();
            Toast.makeText (mContext, "Unable to add markers to map", Toast.LENGTH_LONG).show ();
            return;
        }

        // Log.e ("URL", "MARKERS : " + markers.size ());
        // Log.e ("URL", "HELPER : " + NavigationDrawerActivity.helper.getNumberOfMarkers());

        mClusterManager.addItems(markers);

        mClusterManager.cluster();

        for (MarkersObj mo : markers) {
            final Marker m = mClusterRenderer.getMarker(mo);
            mo.setMarker(m);
            mo.changeIconDefault();
        }

        // To dismiss the dialog
        progress.dismiss();
    }

    private synchronized void draw_route (final Location location) {
        if (location == null)
            return;

        Log.e ("LOCATION", "INSIDE ROUTE");

        activity.runOnUiThread (new Runnable () {
            public synchronized void run () {
                if (aed_marker != null) {
                    LatLng cur = new LatLng (location.getLatitude(), location.getLongitude()), latLng = aed_marker.getMarkerOption ().getPosition();
                    String url = get_directions_url(cur, latLng);

                    if (checkNetworkConnection ())
                        Ion.with(mContext).
                                load(url).

                                asString()
                                .setCallback(
                                        new FutureCallback<String>() {
                                            @Override
                                            public void onCompleted(Exception e, String result) {
                                                if (e != null) {
                                                    Log.e ("Runnable", "Exception");
                                                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show ();
                                                    // error handling goes here
                                                } else {
                                                    Log.e ("Runnable", "Start Drawing Route");
                                                    parserTask = new MapPageFragment.ParserTask();
                                                    parserTask.execute(result);
                                                    Log.e ("Runnable", "Finish Drawing Route");
                                                }
                                            }
                                        }
                                );
                    aed_marker.showInfoWindow ();
                    // closestMarker.changeIconTarget ();
                }
                /* else {
                    aed_marker.changeIconSelected ();
                    map.moveCamera (CameraUpdateFactory.newLatLngZoom (aed_marker.getMarkerOption ().getPosition (), 13));
                } */
                changeButtonText ();
            }
        });
        Log.e ("Runnable", "FINISH DRAW ROUTE");
    }

    private void zoom_current_position (int zoom_lvl) {
        if (NavigationDrawerActivity.mCurrentLocation != null)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng (NavigationDrawerActivity.mCurrentLocation.getLatitude(), NavigationDrawerActivity.mCurrentLocation.getLongitude()), zoom_lvl));
    }

    private synchronized void closest_marker_setup () {
        int locationGrant = 1;

        // get_bin offline runs anyway, redo for efficiency

        if (mContext == null) {
            Log.e ("Runnable", "No Context");
            return;
        }

        if (NavigationDrawerActivity.mRequestingLocationUpdates && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) || !location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            locationGrant = 0;

        // if (!location_manager.isProviderEnabled( LocationManager.GPS_PROVIDER)
           // buildAlertMessageNoGps();
        // }

        if (markers == null || markers.isEmpty () || markers.size () == 0) {
            Log.e ("Runnable", "Markers Empty");
            return;
        }

        boolean check = markers.isEmpty();

        Log.e ("MARKERS", check + "");

        // Future, check if aed_marker is same as closest marker and if so, do not change it.
        if (locationGrant == 1) {
            Log.e ("Runnable", "Where Am I");
            whereAmI ();
            Log.e ("Runnable", "Wait for location");
            long startTime = System.currentTimeMillis ();
            while (NavigationDrawerActivity.mCurrentLocation == null && System.currentTimeMillis () - startTime < USER_LOCATION_TIMEOUT) {}
            // Log.e (":)", "WAITING");
            Log.e ("Runnable", "Location is ready");
            if (closestMarker != null)
                activity.runOnUiThread (new Runnable () {
                    @Override
                    public void run () {
                        closestMarker.changeIconTarget();
                    }
                });
        }

        if (checkNetworkConnection()) {
            if (locationGrant == 1 && get_closest_bin_online () == 0) {
                long startTime = System.currentTimeMillis (), finishTime = 0;
                while (closest_marker_lock && (finishTime = System.currentTimeMillis () - startTime) < USER_LOCATION_TIMEOUT) {}
                if (finishTime >= USER_LOCATION_TIMEOUT) {
                    closest_marker_setup ();
                    return;
                }
                if (aed_marker == null)
                    aed_marker = closestMarker;
            } else if (aed_marker == null) {
                Log.e ("MARKERS", check + "");
                aed_marker = markers.get(0);
            }
        } else {
            int marker_ndx = get_closest_bin();
            if (marker_ndx == -1 && aed_marker == null)
                aed_marker = markers.get (0);
            else {
                closestMarker = markers.get(marker_ndx);
                if (aed_marker == null)
                    aed_marker = closestMarker;
            }
        }

        if (aed_marker == null && !markers.isEmpty ())
            aed_marker = markers.get (0);

        Log.e ("Runnable", "Closest Marker Assigned");

        if (NavigationDrawerActivity.mCurrentLocation == null)
            Log.e ("Runnable", "Current Location null");

        draw_route (NavigationDrawerActivity.mCurrentLocation);
        activity.runOnUiThread (new Runnable () {
            public void run () {
                if (closestMarker != null) {
                    if (aed_marker != null && aed_marker.getPosition().equals (closestMarker.getPosition ())) {
                        // mClusterManager.removeItem (aed_marker);
                        // aed_marker.changeIconSelected();
                        // aed_marker.setIcon (MarkersObj.icon_selected);

                        mClusterManager.removeItem(closestMarker);
                        mClusterManager.removeItem(aed_marker);
                        aed_marker.changeIconSelected ();
                        closestMarker.changeIconSelected ();
                        mClusterManager.addItem (aed_marker);
                        // mClusterManager.addItem (closestMarker);
                        mClusterManager.cluster();
                        // mClusterManager.cluster ();
                    } else {
                        mClusterManager.removeItem(closestMarker);
                        closestMarker.setIcon (MarkersObj.icon_target);
                        closestMarker.setIcon (MarkersObj.icon_target);
                        mClusterManager.addItem(closestMarker);
                        mClusterManager.cluster();
                    }
                }
                zoom_current_position (13);
                // zoom_current_position (1);
            }
        });
    }

    private boolean checkNetworkConnection () {
        ConnectivityManager cm =
                (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    // Creates and returns the directions url
    private String get_directions_url (LatLng origin, LatLng dest) {

        String str_origin = "origin=" + origin.latitude + "," + origin.longitude, str_dest = "destination=" + dest.latitude + "," + dest.longitude, sensor = "sensor=false", parameters = str_origin + "&" + str_dest + "&" + sensor;

        return "https://maps.googleapis.com/maps/api/directions/json?" + parameters;

    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line;
            while((line = br.readLine ())  != null)
                sb.append(line);

            data = sb.toString();

            br.close();

        } catch(Exception e){
            Log.d("Exception while url", e.toString ());
        } finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;

            for(int i=0;i<result.size();i++){
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for(int j=0; j < path.size();++j) {
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat")), lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(4);
                lineOptions.color(Color.RED); // MAYBE CHANGE TO BLUE BEFORE RELEASE????
            }

            if (route != null)
                route.remove ();

            if (lineOptions != null) {
                route = map.addPolyline(lineOptions);

                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        route.setWidth(cameraPosition.zoom < 13 ? 10 : 4);
                    }

                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e (":)", "ON RESUME");
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        network_looper = handler_thread.getLooper();
        network_handler = new Handler(network_looper);
        mContext.registerReceiver(networkStateReceiver, filter, null, network_handler); // Will not run on main thread
        // Within {@code onPause()}, we remove location updates. Here, we resume receiving
        // location updates if the user has requested them.
        run_timer();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (route != null)
            route.remove ();
        closestMarker = null;
        aed_marker    = null;
        Log.e (":)", "ON PAUSE");
        // network_looper.quit();
        network_handler.removeCallbacksAndMessages(null);
        try {
            mContext.unregisterReceiver(networkStateReceiver);
        } catch (Exception ex) {}
        try {
            stop_timer ();
        } catch (Exception ex) {}

        try {
            Intent mServiceIntent = new Intent (mContext, BackendPullService.class);
            mContext.stopService (mServiceIntent);
        } catch (Exception ex) {}
        try {
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(backendReceiver);
        } catch (Exception ex) {}
        // Remove location updates to save battery.
    }

    @Override
    public void onStop () {
        super.onStop ();
        Log.e (":)", "ON STOP");
        try {
            mContext.unregisterReceiver(networkStateReceiver);
            network_handler.removeCallbacksAndMessages(null);
            stop_timer();
        } catch (Exception ex) {

        }
    }

    private class BackendPullServiceReceiver extends BroadcastReceiver {

        private BackendPullServiceReceiver() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra (Constants.EXTENDED_DATA_STATUS, Constants.STATE_ACTION_CONNECTING);
            if (status == Constants.STATE_ACTION_COMPLETE) {
                if (isAdded ()) {
                    // getLoaderManager().initLoader(0, null, new MapPageFragment.MarkersLoaderCallbacks());
                    workerFetch = new Thread (workerFetchRunnable);
                    workerFetch.start ();
                    try {
                        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(backendReceiver);
                    } catch (Exception ex) {

                    }
                }
            } else if (status == Constants.STATE_ACTION_FAILED) {
                Toast.makeText (mContext, "Failed to fetch markers", Toast.LENGTH_LONG).show ();

                if (progress != null) {
                    progress.dismiss ();
                    progress = null;
                }

                try {
                    LocalBroadcastManager.getInstance(mContext).unregisterReceiver(backendReceiver);
                } catch (Exception ex) {

                }
            }
        }
    }

    private class WhereAmIServiceReceiver extends BroadcastReceiver {

        private WhereAmIServiceReceiver () {

        }

        @Override
        public void onReceive (Context context, Intent intent) {
            int status = intent.getIntExtra (Constants.EXTENDED_DATA_STATUS, Constants.STATE_ACTION_CONNECTING);
            if (status == Constants.STATE_ACTION_COMPLETE) {
                if (isAdded ())
                    activity.runOnUiThread (new Runnable () {
                        @Override
                        public void run () {
                            ChangeWhereAmIText();
                        }
                    });
                try {
                    // unregisterReceiver(this);
                    LocalBroadcastManager.getInstance (mContext).unregisterReceiver (whereAmIReceiver);
                } catch (Exception ex) {}
            }
        }

    }

    /*
    private class MarkersLoaderCallbacks implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public android.support.v4.content.Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
            return new android.support.v4.content.CursorLoader(mContext, MarkersContentProvider.MARKERS_URI, null, null, null, null);
        }

        @Override
        public void onLoadFinished(final android.support.v4.content.Loader<Cursor> loader, final Cursor data) {
            aed_cursor = data;
            Log.e ("URL", "CURSOR SIZE ORIGINAL : " + aed_cursor.getCount());
            Log.e ("URL", "DATA SIZE ORIGINAL : "   + data.getCount());
            workerFetch = new Thread (workerFetchRunnable);
            workerFetch.start ();
        }

        @Override
        public void onLoaderReset(final android.support.v4.content.Loader<Cursor> loader) {

        }
    } */

}
