package ternary.defiblocatorprototype;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

public class DonateFragment extends Fragment {

    private Context mContext;

    private WebView mWebView;

    private ProgressWebViewClient progressWebViewClient;

    public DonateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment DonateFragment.
     */
    public static DonateFragment newInstance() {
        DonateFragment fragment = new DonateFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_donate, container, false);
        mWebView = v.findViewById(R.id.donate_web_view);

        ProgressBar progressBar = v.findViewById (R.id.donate_page_progress_bar);

        progressWebViewClient = new ProgressWebViewClient(progressBar);

        mWebView.loadUrl (mContext.getString (R.string.donate_url));

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(progressWebViewClient);

        return v;

    }

    @Override
    public void onActivityCreated(Bundle args) {
        super.onActivityCreated (args);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

}
