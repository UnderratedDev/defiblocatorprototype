package ternary.defiblocatorprototype;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by yudhvirraj on 2017-09-07.
 */


public class DisclaimerDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.disclaimer_title)
                .setMessage(R.string.disclaimer)
                .setCancelable(false)
                .setPositiveButton(R.string.agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // and, if the user accept, you can execute something like this:
                        // We need an Editor object to make preference changes.
                        // All objects are from android.context.Context
                        NavigationDrawerActivity.editor.putBoolean ("accepted", true);
                        NavigationDrawerActivity.editor.apply ();
                    }
                })
                .setNegativeButton("Disagree", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.exit(0);
                    }
                });
        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog ().setCanceledOnTouchOutside (false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}