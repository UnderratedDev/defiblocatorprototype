package underrateddev.defiblocatorwatch;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import underrateddev.defiblocatorwatch.database.DatabaseHelper;
import underrateddev.defiblocatorwatch.database.schema.Markers;

public class MainActivity extends FragmentActivity {

    private DatabaseHelper         helper;
    private Cursor                 cursor;
    private Runnable               runnable;
    private Thread                 thread;
    private ArrayList <MarkersObj> markers;
    private boolean                closest_marker_lock;
    private MarkersObj             closest_marker;
    private boolean                location_granted;
    private PriorityQueue<HashMap<MarkersObj, Float>> closestMarkers = new PriorityQueue<>(1, new Comparator<HashMap<MarkersObj, Float>>() {

        @Override
        public int compare(HashMap<MarkersObj, Float> map, HashMap<MarkersObj, Float> map1) {
            Map.Entry<MarkersObj, Float> entry = map.entrySet().iterator().next(), entry1 = map1.entrySet().iterator().next();

            return map.get(entry.getKey()).compareTo(map1.get(entry1.getKey()));
        }
    });

    private MainActivity.BackendPullServiceReceiver backend_receiver;

    public final static int MY_PERMISSIONS_REQUEST_ACCESS_ALL = 10, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1, MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 2, MY_PERMISSIONS_REQUEST_ACCESS_CALL_PHONE = 3;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 500;

    /**
     * The desired distance for location updates. Inexact.
     */
    private static final long LOCATION_REFRESH_DISTANCE       = 100;


    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private LocationManager location_manager;

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            currentLocation = location;
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    /**
     * Represents a geographical location.
     */
    private Location currentLocation;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    private Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    private String mLastUpdateTime;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setup_db();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (cursor != null) {
                    extractCursorData();

                    // get_bin offline runs anyway, redo for efficiency

                    if (getBaseContext() == null) {
                        Log.e("Runnable", "No Context");
                        return;
                    }

                    if ((ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) || !location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        Toast.makeText(getBaseContext(), "No Location", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (location_granted)
                        while (currentLocation == null)
                            Log.e(":)", "Waiting for user location");
                    get_closest_bin_online();
                }
            }
        };
        pullDataFromServer();
        setContentView(R.layout.activity_main);
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        // Update values using data stored in the Bundle.
        // updateValuesFrom
        // Bundle(savedInstanceState);

        if (check_location_permission() && !checkCallPermission())
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_ACCESS_ALL);
        else {
            if (check_location_permission())
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            else
                init_location();
            if (!checkCallPermission())
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_ACCESS_CALL_PHONE);
        }
    }

    private void init_location () {
        if (ActivityCompat.checkSelfPermission(getBaseContext ().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext ().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;
        location_manager = (LocationManager) getSystemService(LOCATION_SERVICE);

        location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS,
                LOCATION_REFRESH_DISTANCE, mLocationListener);

        location_granted = true;
    }

        // Check for location permission (FINE AND COARSE) and returns true if permission has not been granted
    private boolean check_location_permission () {
        return (ActivityCompat.checkSelfPermission(getBaseContext (), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext (), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkCallPermission () {
        return (getBaseContext ().checkCallingOrSelfPermission ("android.permission.CALL_PHONE") == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_ALL : {
                init_location();
                break;
            }
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    init_location();
                break;
            }
            case MY_PERMISSIONS_REQUEST_ACCESS_CALL_PHONE: {
                break;
            }
        }
    }

    private void pullDataFromServer () {
        Intent mServiceIntent = new Intent (getBaseContext (), BackendPullService.class);
        mServiceIntent.setData (Uri.parse ("http://defiblocator.ca/api/get_markers.php"));
        getBaseContext().startService (mServiceIntent);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.BROADCAST_ACTION);
        backend_receiver = new MainActivity.BackendPullServiceReceiver();

        LocalBroadcastManager.getInstance(getBaseContext ()).registerReceiver(backend_receiver, intentFilter);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    public void onDestroy () {
        super.onDestroy ();
        close_db ();
    }

    public void setup_db () {
        helper = DatabaseHelper.getInstance (this);
        helper.openDatabaseForWriting (this);
    }

    public void close_db () {
        if (helper != null)
            helper.close ();
    }

    private void extractCursorData () {
        setup_db ();
        markers = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast (); cursor.moveToNext ()) {
            final Markers marker = helper.getMarkerFromCursor (cursor);
            markers.add (new MarkersObj (marker));
        }
        Log.e (":)", "Markers Size: " + markers.size ());
        close_db ();
    }

    // Returns the index of a bin in the markers list
    private int getMarker (final MarkersObj m) {
        for (int i = 0; i < markers.size (); ++i)
            if (markers.get(i).getMarkersObject ().getId () == m.getMarkersObject ().getId ())
                return i;
        return -1;
    }

    private String get_distance_url (LatLng origin, ArrayList<MarkersObj> dest) {
        String str_dest = "destinations=";
        for (int i = 0; i < dest.size(); i++) {
            str_dest = str_dest + dest.get(i).getMarkerOption().getPosition().latitude + "," + dest.get(i).getMarkerOption().getPosition().longitude;
            str_dest = i != dest.size() - 1 ? str_dest + "|" : str_dest ;
        }

        String str_origin = "origins=" + origin.latitude + "," + origin.longitude, parameters = str_origin + "&" + str_dest;

        return "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&" + parameters;
    }

    // Calculates the closest bin relative to cur user position/location
    private int get_closest_bin () {
        int index = -1;
        float minDistance = Float.MAX_VALUE;
        final Location location = currentLocation;

        if (location == null || markers.isEmpty())
            return index;

        if (0 < markers.size ()) {
            Location target = new Location ("target");
            for (int i = 0; i < markers.size(); ++i) {
                LatLng temp = markers.get (i).getMarkerOption().getPosition ();
                target.setLatitude  (temp.latitude);
                target.setLongitude (temp.longitude);
                if (location.distanceTo (target) < minDistance) {
                    minDistance = location.distanceTo(target);
                    index = i;
                }
            }
        }
        return index;
    }

    private int get_closest_bin_online () {

        while (closest_marker_lock)
            Log.e (":)", "Waiting for turn");

        closest_marker_lock = true;

        final Location location = currentLocation;

        if (location == null || markers.isEmpty())
            return 1;

        Log.e (":)", "markers not empty and location not null");

        Location target = new Location ("target");

        closestMarkers.clear ();

        for (MarkersObj mo :  markers) {
            LatLng temp = mo.getMarkerOption().getPosition ();
            target.setLatitude  (temp.latitude);
            target.setLongitude (temp.longitude);
            HashMap<MarkersObj, Float> hm = new HashMap <>();
            hm.put (mo, location.distanceTo(target));
            closestMarkers.add (hm);
        }

        final ArrayList<MarkersObj> closest = new ArrayList<>();

        int no_distance_markers = 5 < closestMarkers.size () ? 5 : closestMarkers.size ();

        for (int i = 0; i < no_distance_markers; i++)
            closest.add(closestMarkers.poll().entrySet().iterator().next().getKey());

        String url = get_distance_url (new LatLng(location.getLatitude(), location.getLongitude()), closest);

        Log.e (":)", url);

        /* Redo in future to prevent java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
           at ternary.defiblocatorprototype.MapPageFragment$13.onCompleted(MapPageFragment.java:954)
           at ternary.defiblocatorprototype.MapPageFragment$13.onCompleted(MapPageFragment.java:947)
         */
        Ion.with(getBaseContext ()).
                load(url).
                asJsonObject()
                .setCallback(
                        new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e != null) {
                                    closest_marker_lock = false;
                                    // error handling goes here
                                } else {
                                    try {
                                        int smallest = Integer.MAX_VALUE, index = 0;
                                        JsonArray array = result.getAsJsonObject().get("rows").getAsJsonArray().get(0).getAsJsonObject().get("elements").getAsJsonArray();

                                        if (!array.get(0).getAsJsonObject().get("status").getAsString().equalsIgnoreCase("ZERO_RESULTS")) {

                                            for (int i = 0; i < array.size(); ++i) {
                                                int current = array.get(i).getAsJsonObject().get("distance").getAsJsonObject().get("value").getAsInt();
                                                if (current < smallest) {
                                                    smallest = current;
                                                    index = i;
                                                }
                                            }

                                            closest_marker = markers.get(getMarker(closest.get(index)));
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace ();
                                        Log.e (":)", "Exceeded Limit!");
                                        int index = get_closest_bin();
                                        if (index != -1)
                                            closest_marker = markers.get(index);
                                    }
                                }
                                closest_marker_lock = false;
                            }
                        }
                );
        return 0;
    }

    public void find_aed (final View view) {
        if (closest_marker != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://google.navigation:///?q=" + closest_marker.getMarkersObject().getLat() + "," + closest_marker.getMarkersObject().getLng() + "&mode=w"));
            // intent.setData(Uri.parse("google.navigation:///?q=48.649469,-2.02579&mode=w"));
            // intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);

        } else {
            Toast.makeText(getBaseContext(), "NULL VALUE", Toast.LENGTH_LONG).show();
        }
    }

    public void call_aed (final View view) {
        if (closest_marker != null) {
            Uri number = Uri.parse("tel:" + closest_marker.getMarkersObject().getContact_number());
            Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
            startActivity(callIntent);
        }
    }

    private class BackendPullServiceReceiver extends BroadcastReceiver {

        private BackendPullServiceReceiver() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra (Constants.EXTENDED_DATA_STATUS, Constants.STATE_ACTION_CONNECTING);
            if (status == Constants.STATE_ACTION_COMPLETE) {
                getSupportLoaderManager().initLoader(0, null, new MainActivity.MarkersLoaderCallbacks());
                try {
                    LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(backend_receiver);
                } catch (Exception ex) {

                }
            } else if (status == Constants.STATE_ACTION_FAILED) {
                if (helper != null && helper.getNumberOfMarkers() > 0)
                    Toast.makeText (getBaseContext (), "Unable to fetch latest aed's, not using latest markers", Toast.LENGTH_LONG).show ();
                getSupportLoaderManager().initLoader(0, null, new MainActivity.MarkersLoaderCallbacks());
                try {
                    // unregisterReceiver(this);
                    LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(backend_receiver);
                } catch (Exception ex) {

                }
            }
        }
    }

    private class MarkersLoaderCallbacks implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {
        @Override
        public android.support.v4.content.Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
            return new android.support.v4.content.CursorLoader(getBaseContext (), MarkersContentProvider.MARKERS_URI, null, null, null, null);
        }

        @Override
        public void onLoadFinished(final android.support.v4.content.Loader<Cursor> loader, final Cursor data) {
            cursor = data;
            thread = new Thread (runnable);
            thread.start ();
        }

        @Override
        public void onLoaderReset(final android.support.v4.content.Loader<Cursor> loader) {

        }
    }
}
