package underrateddev.defiblocatorwatch;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import underrateddev.defiblocatorwatch.database.schema.Markers;

/**
 * Created by Yudhvir on 15/06/17.
 */

public class MarkersObj {

    private MarkerOptions mo;
    private Marker        ma;
    private Markers       m;
    private BitmapDescriptor previous, current;

    /*
        ESA MODIFY THIS TO ADD MORE THAN JUST ADDRESS, ADD ADDRESS ONE, POST CODE ETC... (MODIFY SNIPPER NOT THE TITLE)
    */
    public MarkersObj(Markers m) {
        this.m  = m;
        this.mo = new MarkerOptions ()
                .title (m.getName ())
                .snippet (m.getAddress ())
                .position (new LatLng(m.getLat(), m.getLng()));
    }

    public MarkersObj(Markers m, BitmapDescriptor icon) {
        this.m  = m;
        this.mo = new MarkerOptions ()
                .title (m.getName ())
                .snippet (m.getAddress ())
                .position (new LatLng(m.getLat(), m.getLng()))
                .icon (icon);
        this.current = icon;
    }

    public MarkerOptions getMarkerOption () {
        return mo;
    }

    public Markers getMarkersObject () {
        return m;
    }

    public void setMarker (final Marker m) {
        this.ma = m;
    }

    public Marker getMarker () {
        return ma;
    }

    public void setMarkerOption (final MarkerOptions mo) {
        this.mo = mo;
    }

    public void setMarker (final Markers m) {
        this.m = m;
    }

    public void changeMarker (final Marker m) {
        this.ma = m;
        // mo.title (m.getTitle ())
           //     .snippet (m.getSnippet ())
           //     .position (m.getPosition ());
    }

    public void removeMarker () {
        if (this.ma != null)
            this.ma.remove ();
        this.ma = null;
    }

    public void setIcon (final BitmapDescriptor icon) {
        this.previous = this.current;
        this.current  = icon;
    }

    public BitmapDescriptor getPreviousIcon () {
        return this.previous;
    }

    public BitmapDescriptor getCurrentIcon () {
        return this.current;
    }

    public void changeIcon (final BitmapDescriptor icon) {
        if (mo == null || ma == null)
            return;
        this.previous = current;
        mo.icon (icon);
        // Log.e (":)", ma.toString ());
        ma.setIcon (icon);
        this.current = icon;
    }

    public void showInfoWindow () {
        if (this.ma != null)
            this.ma.showInfoWindow ();
    }
}
