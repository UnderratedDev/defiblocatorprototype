package underrateddev.defiblocatorwatch;

/**
 * Created by Yudhvir on 2017-02-12.
 */

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import underrateddev.defiblocatorwatch.database.DatabaseHelper;

public class MarkersContentProvider
        extends ContentProvider
{
    private static final UriMatcher uriMatcher;
    private static final int MARKERS_URI_INT = 1;
    public  static final Uri MARKERS_URI;
    private DatabaseHelper helper;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI ("underrateddev.defiblocatorwatch.database.schema", "/Markers", MARKERS_URI_INT);
    }

    static
    {
        MARKERS_URI = Uri.parse ("content://underrateddev.defiblocatorwatch.database.schema/Markers");
    }

    @Override
    public boolean onCreate()
    {
        helper = DatabaseHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(final Uri uri,
                        final String[] projection,
                        final String selection,
                        final String[] selectionArgs,
                        final String sortOrder)
    {
        final Cursor cursor;

        switch (uriMatcher.match(uri))
        {

            case MARKERS_URI_INT:
            {
                helper.openDatabaseForReading(getContext());
                cursor = helper.getMarkersCursor();
                helper.close();
                break;
            }

            default:
            {
                throw new IllegalArgumentException("Unsupported URI: " + uri);
            }
        }

        return (cursor);
    }

    @Override
    public String getType(final Uri uri)
    {
        final String type;

        switch(uriMatcher.match(uri))
        {
            case MARKERS_URI_INT:
                type = "vnd.android.cursor.dir/vnd.ternary.defiblocatorprototype.database.schema.Markers";
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        return (type);
    }

    @Override
    public int delete(final Uri uri,
                      final String selection,
                      final String[] selectionArgs)
    {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(final Uri uri,
                      final ContentValues values)
    {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(final Uri uri,
                      final ContentValues values,
                      final String selection,
                      final String[]      selectionArgs)
    {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
