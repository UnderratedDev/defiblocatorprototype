package underrateddev.defiblocatorwatch;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import underrateddev.defiblocatorwatch.database.DatabaseHelper;
import underrateddev.defiblocatorwatch.database.schema.Markers;

/**
 * Created by Yudhvir on 10/05/2017.
 */

public class BackendPullService extends IntentService {

    private DatabaseHelper helper;
    private ArrayList<Markers> markers;

    private BroadcastNotifier broadcaster;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * name Used to name the worker thread, important only for debugging.
     */
    public BackendPullService() {
        super("BackendPullService");
        helper = DatabaseHelper.getInstance (this);
    }

    private void insertIntoDatabase () {
        helper.openDatabaseForWriting (this);
        helper.delete_all_markers ();
        Markers[] markerArr = new Markers[markers.size()];
        markers.toArray(markerArr);
        helper.createMarkerObjFromArray (markerArr);
        helper.close ();
        // for (Markers m : markers)
           // helper.insertMarker (m);
    }

    private void getAll () {
        helper.openDatabaseForReading(this);
        Cursor markersCursor = helper.getMarkersCursor();
        for (markersCursor.moveToFirst(); !markersCursor.isAfterLast (); markersCursor.moveToNext ())
            Log.e ("SERVICE", helper.getMarkerFromCursor(markersCursor).toString ());
        helper.close ();
    }

    @Override
    protected void onHandleIntent (Intent workIntent) {
        markers = new ArrayList<>();
        broadcaster = new BroadcastNotifier(this);
        // getAll ();
        String dataString = workIntent.getDataString ();
        Log.e ("Background Service", dataString);
        Ion.with(getApplicationContext()).
                load(dataString).
                asJsonArray()
                .setCallback(
                        new FutureCallback<JsonArray>() {
                            @Override
                            public void onCompleted(Exception e, JsonArray array) {
                                if (e != null) {
                                    Toast.makeText (getApplicationContext (), "Unable to fetch markers", Toast.LENGTH_SHORT).show ();
                                    Log.e(":)", e.toString ());
                                    broadcaster.broadcastIntentWithState(Constants.STATE_ACTION_FAILED);
                                    // error handling goes here
                                } else {

                                    for (final JsonElement el : array) {
                                        final JsonObject json;
                                        final JsonElement latitude_element, longtitude_element, name_element, address_element, addressone_element, city_element, country_element, province_element, postalcode_element, contactfname_element, contactlname_element, contactnumber_element, submitfname_element, submitlname_element, submitnumber_element;

                                        final String name, address, addressone, city, country, province, postalcode, contactfname, contactlname, contactnumber, submitfname, submitlname, submitnumber;
                                        final double latitude, longtitude;

                                        json                  = el.getAsJsonObject();
                                        contactfname_element  = json.get("ContactFName");
                                        contactlname_element  = json.get("ContactLName");
                                        contactnumber_element = json.get("ContactNumber");
                                        submitfname_element   = json.get("SubmittedFName");
                                        submitlname_element   = json.get("SubmittedLName");
                                        submitnumber_element  = json.get("SubmittedNumber");
                                        name_element          = json.get("Name");
                                        address_element       = json.get("Address");
                                        addressone_element    = json.get("Address1");
                                        city_element          = json.get("City");
                                        country_element       = json.get("Country");
                                        province_element      = json.get("Province");
                                        postalcode_element    = json.get("PostalCode");
                                        latitude_element      = json.get("Latitude");
                                        longtitude_element    = json.get("Longitude");

                                        contactfname          = contactfname_element.getAsString ();
                                        contactlname          = contactlname_element.getAsString ();
                                        contactnumber         = contactnumber_element.getAsString ();
                                        submitfname           = submitfname_element.getAsString ();
                                        submitlname           = submitlname_element.getAsString ();
                                        submitnumber          = submitnumber_element.getAsString ();
                                        name                  = name_element.getAsString ();
                                        address               = address_element.getAsString();
                                        addressone            = addressone_element.getAsString ();
                                        city                  = city_element.getAsString();
                                        country               = country_element.getAsString();
                                        province              = province_element.getAsString();
                                        postalcode            = postalcode_element.getAsString();
                                        latitude              = latitude_element.getAsDouble ();
                                        longtitude            = longtitude_element.getAsDouble ();

                                        // Log.e (":)", contactfname + " " + contactlname + " " + contactnumber + " " + submitfname + " " + submitlname + " " + submitnumber
                                           //     + " " + name + " " + address+ " " + latitude + " " + longtitude);

                                        markers.add (new Markers (null, latitude, longtitude, name, address, addressone, city, country, province, postalcode, contactfname, contactlname, contactnumber, submitfname, submitlname, submitnumber));
                                    }
                                    insertIntoDatabase ();
                                    // getAll ();
                                    broadcaster.broadcastIntentWithState(Constants.STATE_ACTION_COMPLETE);
                                }
                            }
                        }
                );

    }
}
